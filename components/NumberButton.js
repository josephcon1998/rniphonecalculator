import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

import { sendNumberToReducer, clearScreenValue, resetButtonStates } from '../redux/actions';

class NumberButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  _handleNumberPress() {
    // 0-9 and '.'
    let { number, divideButtonState, multiplyButtonState, subtractButtonState, addButtonState, clearScreenValue, resetButtonStates } = this.props

    // if the user taps a number after they've tapped an operation
    if(divideButtonState || multiplyButtonState || subtractButtonState || addButtonState) {
      clearScreenValue();
      resetButtonStates();
    }

    switch(number) {
      case 0: { this.props.sendNumberToReducer(0); break; }
      case 1: { this.props.sendNumberToReducer(1); break; }
      case 2: { this.props.sendNumberToReducer(2); break; }
      case 3: { this.props.sendNumberToReducer(3); break; }
      case 4: { this.props.sendNumberToReducer(4); break; }
      case 5: { this.props.sendNumberToReducer(5); break; }
      case 6: { this.props.sendNumberToReducer(6); break; }
      case 7: { this.props.sendNumberToReducer(7); break; }
      case 8: { this.props.sendNumberToReducer(8); break; }
      case 9: { this.props.sendNumberToReducer(9); break; }
      case '.': { this.props.sendNumberToReducer('.'); break; }
    }
  }

  render() {
    return (
      <TouchableOpacity onPress={() => {this._handleNumberPress()}}>
        <View style={[styles.rootContainer, {width: this.props.number === 0 ? 160 : 75}]}>
          <Text 
            style={[styles.number, {alignSelf: this.props.number === 0 ? 'flex-start' : 'center', paddingLeft: this.props.number === 0 ? 20 : 0}]}
          >
            {this.props.number}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const mapDispatchToProps = {
  sendNumberToReducer,
  clearScreenValue,
  resetButtonStates,
}

const mapStateToProps = state => ({
  divideButtonState: state.outputReducer.divideButtonState,
  multiplyButtonState: state.outputReducer.multiplyButtonState,
  subtractButtonState: state.outputReducer.subtractButtonState,
  addButtonState: state.outputReducer.addButtonState,
})

const styles = StyleSheet.create({
  rootContainer: {
    width: 75,
    height: 75,
    borderRadius: 37.5,
    backgroundColor: '#333333',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
  },
  number: {
    color: 'white',
    fontSize: 30,
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(NumberButton);
