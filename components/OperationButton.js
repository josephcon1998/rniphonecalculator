import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import { connect } from 'react-redux';
import { updateScreenValue, pushWorkingValue, clearAll, clearScreenValue, clearWorkingValues, switchSigns, convertToPercent, toggleButtonActivation } from '../redux/actions';

const OPERATOR_BUTTON_INACTIVE_COLOR = '#FA9324'
const OPERATOR_BUTTON_ACTIVE_COLOR = '#FFFFFF'
const ALT_OPERATOR_BUTTON_COLOR = '#AAAAAA'

const DEFAULT_TEXT_COLOR = '#FFFFFF'
const ALT_OPERATOR_TEXT_COLOR = '#000000'
const OPERATOR_ACTIVE_TEXT_COLOR = '#FA9324'

class OperationButton extends Component {
  constructor(props) {
    super(props);
  }

  // Performs the arithemtic for each operation
  _performMathOperations() {
    let { lastKnownButtonOperationState, workingValues, updateScreenValue } = this.props;
    
    switch(lastKnownButtonOperationState) {
      case 'divide' : { updateScreenValue(String(workingValues[0] / workingValues[1])); break; }
      case 'multiply' : { updateScreenValue(String(workingValues[0] * workingValues[1])); break; }
      case 'subtract' : { updateScreenValue(String(workingValues[0] - workingValues[1])); break; }
      case 'add' : { updateScreenValue(String((Number(workingValues[0]) + Number(workingValues[1])))); break; }
      default: { updateScreenValue('Error'); }
    }
  }

  // Called when equal button is pressed
  _equals() {
    let { screenValue, pushWorkingValue } = this.props;
    
    this._performMathOperations(() => {
      // Push the value that is on the screen
      pushWorkingValue(Number(screenValue))
    })

  }

  // Calulate Operations (+,-,/,*)
  _calculate(operation) { 
    let { pushWorkingValue, workingValues, screenValue, toggleButtonActivation, clearWorkingValues } = this.props;

    // Push screen value to working array
    if(workingValues[0] === undefined || workingValues[1] === undefined) {
        pushWorkingValue(screenValue)
    }
    else {
        updateScreenValue(this._performMathOperations());
        clearWorkingValues();
    } 

    // Update screen to show button has been tapped
    switch(operation) {
      case 'divide': { toggleButtonActivation(operation); break; }
      case 'multiply': { toggleButtonActivation(operation); break; }
      case 'subtract': { toggleButtonActivation(operation); break; }
      case 'add': { toggleButtonActivation(operation); break; }
    }
  }

  _handleOperationPress() {
    switch(this.props.operation) {
        case 'clear': { this.props.clearAll(); break; }
        case 'sign': { this.props.switchSigns(); break; }
        case 'percent': { this.props.convertToPercent(); break; }
        case 'divide': { this._calculate('divide'); break; }
        case 'multiply': { this._calculate('multiply'); break; }
        case 'subtract': { this._calculate('subtract'); break; }
        case 'add': { this._calculate('add'); break; }
        case 'equal': { this._performMathOperations(); break; }
    }
  }

  _handleButtonTextType() {
    switch(this.props.operation) {
        case 'clear': { return this.props.screenValue !== '0' ? 'C' : 'AC'}
        case 'sign': { return '\u00B1' }
        case 'percent': { return '%' }
        case 'divide': { return '\u00F7' }
        case 'multiply': { return '\u00D7' }
        case 'subtract': { return '-' }
        case 'add': { return '+' }
        case 'equal': { return '=' }
    }
  }

  _handleButtonBackgroundColor() {
    switch(this.props.operation) {
        case 'divide': { return { backgroundColor: this.props.divideButtonState === false ? OPERATOR_BUTTON_INACTIVE_COLOR : OPERATOR_BUTTON_ACTIVE_COLOR }}
        case 'multiply': { return { backgroundColor: this.props.multiplyButtonState === false ? OPERATOR_BUTTON_INACTIVE_COLOR : OPERATOR_BUTTON_ACTIVE_COLOR }}
        case 'subtract': { return { backgroundColor: this.props.subtractButtonState === false ? OPERATOR_BUTTON_INACTIVE_COLOR : OPERATOR_BUTTON_ACTIVE_COLOR }}
        case 'add': { return { backgroundColor: this.props.addButtonState === false ? OPERATOR_BUTTON_INACTIVE_COLOR : OPERATOR_BUTTON_ACTIVE_COLOR }}
        
        case 'clear': { return { backgroundColor: ALT_OPERATOR_BUTTON_COLOR }}
        case 'sign': { return { backgroundColor: ALT_OPERATOR_BUTTON_COLOR }}
        case 'percent': { return { backgroundColor: ALT_OPERATOR_BUTTON_COLOR }}
    }
  }

  _handleButtonTextColor() {
    switch(this.props.operation) {
        case 'divide': { return { color: this.props.divideButtonState === false ? DEFAULT_TEXT_COLOR : OPERATOR_ACTIVE_TEXT_COLOR }}
        case 'multiply': { return { color: this.props.multiplyButtonState === false ? DEFAULT_TEXT_COLOR : OPERATOR_ACTIVE_TEXT_COLOR }}
        case 'subtract': { return { color: this.props.subtractButtonState === false ? DEFAULT_TEXT_COLOR : OPERATOR_ACTIVE_TEXT_COLOR }}
        case 'add': { return { color: this.props.addButtonState === false ? DEFAULT_TEXT_COLOR : OPERATOR_ACTIVE_TEXT_COLOR }}
        
        case 'clear': { return { color: ALT_OPERATOR_TEXT_COLOR }}
        case 'sign': { return { color: ALT_OPERATOR_TEXT_COLOR }}
        case 'percent': { return { color: ALT_OPERATOR_TEXT_COLOR }}
    }
  }

  render() {
    return (
        <TouchableOpacity onPress={() => this._handleOperationPress()}>
            <View style={[styles.rootContainer, this._handleButtonBackgroundColor()]}>
                <Text style={[styles.operator, this._handleButtonTextColor()]}>{this._handleButtonTextType()}</Text>
            </View>
        </TouchableOpacity>
    );
  }
}

const mapDispatchToProps = {
    clearScreenValue,
    clearWorkingValues,
    clearAll,
    switchSigns,
    convertToPercent,
    toggleButtonActivation,
    pushWorkingValue,
    updateScreenValue,
}

const mapStateToProps = state => ({
    divideButtonState: state.outputReducer.divideButtonState,
    multiplyButtonState: state.outputReducer.multiplyButtonState,
    addButtonState: state.outputReducer.addButtonState,
    subtractButtonState: state.outputReducer.subtractButtonState,
    workingValues: state.outputReducer.workingValues,
    screenValue: state.outputReducer.screenValue,
    lastKnownButtonOperationState: state.outputReducer.lastKnownButtonOperationState,
})

const styles = StyleSheet.create({
    rootContainer: {
      width: 75,
      height: 75,
      borderRadius: 37.5,
      backgroundColor: '#FA9324',
      justifyContent: 'center',
      alignItems: 'center',
      marginRight: 10,
    },
    operator: {
      color: DEFAULT_TEXT_COLOR,
      fontSize: 30,
    },
  })

export default connect(mapStateToProps, mapDispatchToProps)(OperationButton);
