import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

class Output extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.rootContainer}>
        <Text style={styles.outputText}>{this.props.screenValue}</Text>
      </View>
    );
  }
}

const mapStateToProps = state => ({
    screenValue: state.outputReducer.screenValue,
})

const styles = StyleSheet.create({
    rootContainer: {
      margin: 10,
    },
    outputText: {
      color: '#ffffff',
      fontSize: 100,
      fontWeight: '200',
    },
})
export default connect(mapStateToProps, null)(Output);
