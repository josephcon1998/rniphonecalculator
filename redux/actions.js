
// Send the number the user tapped to the screen
export function sendNumberToReducer(number) {
    return {
        type: 'NUMBER_PRESSED',
        payload: number,
    }
}

// Clear all data stored by the reducer
export function clearAll() {
    return {
        type: 'CLEAR_ALL'
    }
}

// Clear only the working values of the reducer
export function clearWorkingValues() {
    return {
        type: 'CLEAR_WORKING_VALUES'
    }
}

// Clear the screen
export function clearScreenValue() {
    return {
        type: 'CLEAR_SCREEN_VALUE'
    }
}

// After calulations, this function updates the screen
// to have the correct answer displayed
export function updateScreenValue(value) {
    return {
        type: 'UPDATE_SCREEN_VALUE',
        payload: value,
    }
}

// Reset only the state of the operation buttons to false
export function resetButtonStates() {
    return {
        type: 'RESET_BUTTON_STATES'
    }
}

// Pushes the value a user has entered onto an array
// to be used with an operation at a later point
export function pushWorkingValue(value) {
    return {
        type: 'PUSH_WORKING_VALUE',
        payload: value,
    }
}

// Tell the reducer to swtich the sign of the number
// on the screen
export function switchSigns() {
    return {
        type: 'SWITCH_SIGNS'
    }
}

// Tell the reducer to convert the current number on 
// the screen to a percent form
export function convertToPercent() {
    return {
        type: 'CONVERT_TO_PERCENT'
    }
}

// Activates or deactivates an operator button
// if the user taps it
export function toggleButtonActivation(operator) {
    return {
        type: 'TOGGLE_BUTTON_ACTIVATION',
        payload: operator
    }
}
