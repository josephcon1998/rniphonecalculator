import { combineReducers } from 'redux';

const initialState_Output = {
    screenValue: '0',
    workingValues: [],
    divideButtonState: false,   // false = inactive
    multiplyButtonState: false,
    addButtonState: false,
    subtractButtonState: false,
    lastKnownButtonOperationState: '',
}

const outputReducer = (state = initialState_Output, action) => {
    switch(action.type) {
        case 'CLEAR_ALL':
            return {
                ...state,
                screenValue: '0',
                workingValues: [],
                divideButtonState: false,
                multiplyButtonState: false,
                addButtonState: false,
                subtractButtonState: false,
                lastKnownButtonOperationState: '',
            }
        case 'CLEAR_WORKING_VALUES':
            return {
                ...state,
                workingValues: [],
            }
        case 'CLEAR_SCREEN_VALUE':
            return {
                ...state,
                screenValue: '0',
            }
        case 'RESET_BUTTON_STATES':
            return {
                ...state,
                divideButtonState: false,
                multiplyButtonState: false,
                addButtonState: false,
                subtractButtonState: false,
            }
        case 'PUSH_WORKING_VALUE':
            return {
                ...state,
                workingValues: [...state.workingValues, action.payload],
            }
        case 'UPDATE_SCREEN_VALUE':
            return {
                ...state,
                screenValue: action.payload,
            }
        case 'NUMBER_PRESSED':
            return {
                ...state,
                screenValue: state.screenValue === '0' ? `${action.payload}` : state.screenValue.concat(`${action.payload}`),
            }
        case 'SWITCH_SIGNS':
            return {
                ...state,
                screenValue: /(-)+/g.test(state.screenValue) === false ? '-' + state.screenValue : state.screenValue.replace('-','')
            }
        case 'CONVERT_TO_PERCENT':
            return {
                ...state,
                screenValue: String(state.screenValue / 100),
            }
        case 'TOGGLE_BUTTON_ACTIVATION':
            switch(action.payload) {
                case 'divide': { return { ...state, divideButtonState: true, multiplyButtonState: false, addButtonState: false, subtractButtonState: false, lastKnownButtonOperationState: 'divide', }}
                case 'multiply': { return { ...state, divideButtonState: false, multiplyButtonState: true, addButtonState: false, subtractButtonState: false, lastKnownButtonOperationState: 'multiply', }}
                case 'subtract': { return { ...state, divideButtonState: false, multiplyButtonState: false, addButtonState: false, subtractButtonState: true, lastKnownButtonOperationState: 'subtract', }}
                case 'add': { return { ...state, divideButtonState: false, multiplyButtonState: false, addButtonState: true, subtractButtonState: false, lastKnownButtonOperationState: 'add', }}
                default: { return { ...state, divideButtonState: false, multiplyButtonState: false, addButtonState: false, subtractButtonState: false, }}
            }
        default:
            return state;
    }
}

export default combineReducers({
    outputReducer,
});
