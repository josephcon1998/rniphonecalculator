import React from 'react';
import { StyleSheet, Text, View, StatusBar, SafeAreaView } from 'react-native';

import NumberButton from './components/NumberButton'
import OperationButton from './components/OperationButton'
import Output from './components/Output';

import { Provider } from 'react-redux';
import configureStore from './redux/store.js';

class App extends React.Component {
  
  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content"/>
        <SafeAreaView>
          
          <View style={styles.outputRow}>
              <Output />
          </View>

          <View style={styles.buttonRow}>
              <OperationButton operation={'clear'} />
              <OperationButton operation={'sign'} />
              <OperationButton operation={'percent'} />
              <OperationButton operation={'divide'} />
          </View>

          <View style={styles.buttonRow}>
              <NumberButton number={7} />
              <NumberButton number={8} />
              <NumberButton number={9} />
              <OperationButton operation={'multiply'}/>
          </View>

          <View style={styles.buttonRow}>
              <NumberButton number={4} />
              <NumberButton number={5} />
              <NumberButton number={6} />
              <OperationButton operation={'subtract'} />
          </View>

          <View style={styles.buttonRow}>
              <NumberButton number={1}/>
              <NumberButton number={2} />
              <NumberButton number={3} />
              <OperationButton operation={'add'} />
          </View>

          <View style={[styles.buttonRow, { marginBottom: 40 }]}>
              <NumberButton number={0}/>
              <NumberButton number={'.'} />
              <OperationButton operation={'equal'} />
          </View>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
  },
  outputRow: {
    flex: 2.5,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  buttonRow: {
    flex: 0.8,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',

  }
});

const store = configureStore();

export default () => (
  <Provider store={store}>
      <App />
  </Provider>
)